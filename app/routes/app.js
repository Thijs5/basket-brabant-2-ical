'use strict';

module.exports = {
  'get /app': {
    action: 'getVersion',
  },
};
