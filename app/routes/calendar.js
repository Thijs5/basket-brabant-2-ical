'use strict';

module.exports = {
  'get /calendars/orp-jauche': {
    action: 'getOrpJaucheCalendar',
  },
};
