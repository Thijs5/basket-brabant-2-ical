'use strict';

const fetch = require('node-fetch');
const cheerio = require('cheerio');
const { DateTime } = require('luxon');
const ical = require('../services/ical');
const packageJson = require('../../package.json');

const formatDate = function (dateText) {
  if (dateText === '-') { return null };
  
  // If the date is filled in, git it.
  const dateRegex = /[0-9\/\s\:]+/gm;
  const dateAsStr = dateRegex.exec(dateText)[0].trim();
  const date = DateTime.fromFormat(dateAsStr, 'dd/LL/yyyy HH:mm', { zone: 'Europe/Brussels' }).toISO();
  return date;
}

const extractGame = function extractGame($) {
  const dateText = $.children(':nth-child(7)').text().trim();
  const date = formatDate(dateText);

  return {
    id: $.children(':nth-child(1)').text().trim(),
    competition: $.children(':nth-child(2)').text().trim(),
    homeTeam: $.children(':nth-child(3)').text().trim(),
    homeScore: $.children(':nth-child(4)').text().trim(),
    visitorsScore: $.children(':nth-child(5)').text().trim(),
    visitors: $.children(':nth-child(6)').text().trim(),
    location: $.children(':nth-child(7)').children('a').attr('href'),
    date,
  }
}

module.exports = ({ config }) => ({
  async getOrpJaucheCalendar (ctx){
    const clubId = '222';
    const url = `https://www.basket-brabant.be/index.php?option=com_aosportleague&view=games&filter_club=${clubId}&Itemid=531`;
    const result = await fetch(url, { method: 'GET' });
    const html = await result.text();
    const $ = cheerio.load(html);

    const games = [];
    $('.table-games tbody tr').filter(function (){
      games.push(extractGame($(this)));
    });

    // ctx.body = html;
    ctx.body = ical.gamesAsCalendar(games);
  },
});
