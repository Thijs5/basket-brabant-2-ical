'use strict';

const packageJson = require('../../package.json');

module.exports = ({ config }) => ({

  async getVersion (ctx) {
    ctx.body = {
        version: packageJson.version,
    } ;
  },
});
