const { DateTime } = require('luxon');
const ical = require('ical-generator');
const moment = require('moment');

const addHours = function addHours(date, n) {
  const dt = DateTime.fromObject(date)
    .plus({ hours: n });
  const d = dt.toJSDate();
  console.log(d);
  return d;
}

const generateDescription = function (game) {
  return `
    N° ${game.id}\n
    Compétition: ${game.competition}\n
    Location: ${game.location}\n
    Résultat: ${game.homeScore} - ${game.visitorsScore}
  `;
}

const gamesAsCalendar = function (games) {
  const calendar = ical({ domain: 'basket-brabant' });
  games
    .filter(game => game.date !== null)
    .map(function (game) {
      calendar.createEvent({
        uid: game.id,
        start: moment(game.date),
        end: moment(game.date).add(2, 'hours'),
        summary: `${game.homeTeam} - ${game.visitors}`,
        description: generateDescription(game),
        location: game.location,
        // url: 'http://sebbo.net/'
      })
    });
  return calendar.toString();
}

module.exports = {
  gamesAsCalendar
};
